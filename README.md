## notify_schedule_to_LINEについて
### 設定手順(ざっくり)
1. Google Sheetsのスクリプトエディタにnotify_schedule_to_LINEをペースト
1. 自分のLINE tokenとGoogle Calendar IDでプレースホルダ部分を置き換え
1. Calendarへのアクセスを許可するために一度実行(このときにLINEに通知したくない場合は送信部分(37~)をコメントアウトする)
1. Google Apps Scriptでトリガーを設定

### 仕様
* 次の動作を実行
	1. 指定したGoogle Calendarから翌日の予定を取得
	1. 予定があった場合はその情報を指定したLINEグループに通知
* トリガー: Google Apps Scriptの設定に準じる

### 制限
* ~1回/日のイベントしか想定していません
* Google Calendarからの取得データによる例外処理はしていません

### 設定者側で必要なデータ
* LINEトークン(<LINE token>)
	1. https://notify-bot.line.me/ja/ にログイン
	1. 右上の名前→「マイページ」
	1. スクロールして「トークンを発行する」
* Google Calendar ID(<Google Calendar ID>)
	* http://www.googleappsscript.info/2017-12-31/calendar_get_and_register.html#tocAnchor-1-2-2

### Google Calendarへのデータ登録について
次のような通知が送信されます。

* `みなさん明日は" + title + "ですよ！\n場所: " + location + "\n時間: " + start_time + "～" + end_time + "\n住所: " + address`
	* ^ addressはGoogle Calendarのイベントの詳細情報欄を拾っています(イベント作成時にGoogle MapのURLを手動でペースト)

したがって必須情報は次のとおりです。

|Google Calendarに設定するイベント情報|通知内容でのデータ名|備考|
|:-----------|:-----------|:-----------
|タイトル|title||
|位置情報|location|スタジオ名+部屋名を想定|
|開始時刻|start_time||
|終了時刻|end_time||
|詳細情報|address|Google MapのURLを手動でペースト|